package com.bhaleraoakshay.task_vadsoft.retrofit_utils.model.user

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserResponse(
    @SerializedName("id") val id: Int,
    @SerializedName("createdAt") val createdAt: String,
    @SerializedName("name") val name: String,
    @SerializedName("avatar") val avatar: String,
    @SerializedName("lastname") val lastname: String,
    @SerializedName("city") val city: String,
    @SerializedName("designation") val designation: String,
    @SerializedName("about") val about: String
): Parcelable