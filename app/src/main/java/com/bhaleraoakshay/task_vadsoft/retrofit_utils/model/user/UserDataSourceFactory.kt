package com.bhaleraoakshay.task_vadsoft.retrofit_utils.model.user

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource

class UserDataSourceFactory : DataSource.Factory<Int, UserResponse>() {
    val userLiveDataSource = MutableLiveData<UserDataSource>()
    override fun create(): DataSource<Int, UserResponse> {
        val userDataSource =
            UserDataSource()
        userLiveDataSource.postValue(userDataSource)
        return userDataSource
    }
}