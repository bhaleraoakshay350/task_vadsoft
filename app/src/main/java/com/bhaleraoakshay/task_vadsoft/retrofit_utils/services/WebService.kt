package com.bhaleraoakshay.task_vadsoft.retrofit_utils.services

import com.bhaleraoakshay.task_vadsoft.retrofit_utils.model.user.UserResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface WebService {

    companion object {
        const val ENDPOINT_POST = "/blogs"
        const val ENDPOINT_USER = "/users"

    }

    @GET(ENDPOINT_USER)
    fun getUsers(@Query("page") page: Int):
            Call<List<UserResponse>>
}