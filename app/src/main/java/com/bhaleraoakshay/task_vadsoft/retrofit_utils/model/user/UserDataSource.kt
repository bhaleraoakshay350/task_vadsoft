package com.bhaleraoakshay.task_vadsoft.retrofit_utils.model.user

import android.util.Log
import androidx.paging.PageKeyedDataSource
import com.bhaleraoakshay.task_vadsoft.retrofit_utils.services.ServiceBuilder
import com.bhaleraoakshay.task_vadsoft.retrofit_utils.services.WebService
import retrofit2.Call
import retrofit2.Response

class UserDataSource : PageKeyedDataSource<Int, UserResponse>() {


    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, UserResponse>
    ) {
        val serviceBuilder = ServiceBuilder.buildWebService(WebService::class.java)

        val call = serviceBuilder.getUsers(FIRST_PAGE)
        call.enqueue(object : retrofit2.Callback<List<UserResponse>> {
            override fun onResponse(
                call: Call<List<UserResponse>>,
                response: Response<List<UserResponse>>
            ) {
                if (response.isSuccessful) {
                    Log.d("response", "onFailure: ${response.body()!!.toString()}")
                    val apiResponse = response.body()!!
                    apiResponse?.let {
                        callback.onResult(apiResponse.toMutableList(), null, FIRST_PAGE + 1)
                    }
                }
            }

            override fun onFailure(call: Call<List<UserResponse>>, t: Throwable) {
                Log.d("response", "onFailure: ${t.localizedMessage}")

            }
        })
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, UserResponse>) {
        val serviceBuilder = ServiceBuilder.buildWebService(WebService::class.java)

        val call = serviceBuilder.getUsers(params.key)
        call.enqueue(object : retrofit2.Callback<List<UserResponse>> {
            override fun onResponse(
                call: Call<List<UserResponse>>,
                response: Response<List<UserResponse>>
            ) {
                if (response.isSuccessful) {
                    val apiResponse = response.body()!!

                    val key = if (params.key > 1) params.key - 1 else 0
                    apiResponse.let {
                        callback.onResult(apiResponse.toMutableList(), key)
                    }
                }
            }

            override fun onFailure(call: Call<List<UserResponse>>, t: Throwable) {
                Log.d("response", "onFailure: ${t.localizedMessage}")
            }
        })
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, UserResponse>) {
        val serviceBuilder = ServiceBuilder.buildWebService(WebService::class.java)
        val call = serviceBuilder.getUsers(params.key)

        call.enqueue(object : retrofit2.Callback<List<UserResponse>> {
            override fun onFailure(call: Call<List<UserResponse>>, t: Throwable) {
                Log.d("response", "onFailure: ${t.localizedMessage}")

            }

            override fun onResponse(
                call: Call<List<UserResponse>>,
                response: Response<List<UserResponse>>
            ) {
                if (response.isSuccessful) {
                    val apiResponse = response.body()

                    val key = if (params.key > 1) params.key - 1 else 0
                    apiResponse?.let {
                        callback.onResult(apiResponse.toMutableList(), key)
                    }
                }
            }

        })
    }

    companion object {
        const val PAGE_SIZE = 10
        const val FIRST_PAGE = 1
    }

}



