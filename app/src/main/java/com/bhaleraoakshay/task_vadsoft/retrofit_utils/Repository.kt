package com.bhaleraoakshay.task_vadsoft.retrofit_utils

import com.bhaleraoakshay.task_vadsoft.retrofit_utils.services.ServiceBuilder
import com.bhaleraoakshay.task_vadsoft.retrofit_utils.services.WebService
import kotlinx.coroutines.Job

object Repository {

    val API = ServiceBuilder.buildWebService(WebService::class.java)
    private var job: Job? = null

    fun cancelJob() {
        if (job != null) {
            if (job!!.isActive)
                job?.cancel()
        }
    }


}