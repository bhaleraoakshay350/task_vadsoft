package com.bhaleraoakshay.task_vadsoft.ui.user_profile

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModel
import com.bhaleraoakshay.task_vadsoft.retrofit_utils.model.user.UserResponse


class UserProfileViewModel : ViewModel() {

    lateinit var  userDataFromBundle: UserResponse
     fun initializeFromBundle(requireArguments: Bundle) {

         userDataFromBundle=UserProfileArgs.fromBundle(requireArguments).user
         Log.d("UserData", "initializeFromBundle: ${userDataFromBundle.about}")

    }

}