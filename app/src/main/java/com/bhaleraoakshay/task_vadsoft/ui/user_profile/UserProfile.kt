package com.bhaleraoakshay.task_vadsoft.ui.user_profile

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bhaleraoakshay.task_vadsoft.MainActivity
import com.bhaleraoakshay.task_vadsoft.R
import com.bhaleraoakshay.task_vadsoft.databinding.UserProfileFragmentBinding
import com.bumptech.glide.Glide

class UserProfile : Fragment() {
    private lateinit var mParentActivity: Activity

    companion object {
        fun newInstance() =
            UserProfile()
    }

    private lateinit var viewModel: UserProfileViewModel
    private lateinit var fragmentUserProfileBinding: UserProfileFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentUserProfileBinding =
            DataBindingUtil.inflate(inflater, R.layout.user_profile_fragment, container, false)

        return fragmentUserProfileBinding.root
    }

    private fun loadUserImage() {
        Glide.with(mParentActivity)
            .load(viewModel.userDataFromBundle.avatar)
            .placeholder(R.drawable.ic_loading)
            .into(fragmentUserProfileBinding.userAvatar)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(UserProfileViewModel::class.java)
        viewModel.initializeFromBundle(requireArguments())
        fragmentUserProfileBinding.mViewModel = viewModel
        loadUserImage()

    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) {
            mParentActivity = context
        }
    }

}