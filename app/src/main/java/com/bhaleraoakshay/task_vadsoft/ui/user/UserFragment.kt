package com.bhaleraoakshay.task_vadsoft.ui.user

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.bhaleraoakshay.task_vadsoft.MainActivity
import com.bhaleraoakshay.task_vadsoft.R
import com.bhaleraoakshay.task_vadsoft.databinding.FragmentUserBinding
import com.bhaleraoakshay.task_vadsoft.retrofit_utils.model.user.UserResponse
import com.bhaleraoakshay.task_vadsoft.ui.adapters.UserAdapter

class UserFragment : Fragment() {

    private lateinit var userViewModel: UserViewModel
    private lateinit var fragmentUserBinding: FragmentUserBinding
    private lateinit var mParentActivity: Activity
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        userViewModel =
            ViewModelProvider(this).get(UserViewModel::class.java)
        fragmentUserBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_user, container, false)
        fragmentUserBinding.lifecycleOwner = this
        navController = Navigation.findNavController(mParentActivity, R.id.nav_host_fragment)
        attachRecyclerViewAdapter()
        return fragmentUserBinding.root
    }

    private fun attachRecyclerViewAdapter() {
        val adapter = UserAdapter { user -> userItemClicked(user) }
        fragmentUserBinding.recyclerView.layoutManager = LinearLayoutManager(mParentActivity)
        val itemViewModel = ViewModelProvider(this)
            .get(UserViewModel::class.java)
        itemViewModel.userPagedList.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })
        fragmentUserBinding.recyclerView.adapter = adapter
        val itemDecoration = DividerItemDecoration(activity, LinearLayoutManager.VERTICAL)
        itemDecoration.setDrawable(getDrawable(mParentActivity, R.drawable.layout_decorator)!!)
        fragmentUserBinding.recyclerView.addItemDecoration(itemDecoration)
    }

    private fun userItemClicked(user: UserResponse) {
        Toast.makeText(mParentActivity, "Clicked: ${user.name}", Toast.LENGTH_SHORT).show()
        val action = UserFragmentDirections.actionUserFragmentToUserProfileFragment(user)
        navController.navigate(action)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) {
            mParentActivity = context
        }
    }


}
