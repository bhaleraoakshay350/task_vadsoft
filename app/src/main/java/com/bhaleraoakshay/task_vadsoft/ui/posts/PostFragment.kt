package com.bhaleraoakshay.task_vadsoft.ui.posts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bhaleraoakshay.task_vadsoft.R
import com.bhaleraoakshay.task_vadsoft.databinding.FragmentPostBinding

class PostFragment : Fragment() {

    private lateinit var postViewModel: PostViewModel
    private lateinit var fragmentPostBinding: FragmentPostBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        postViewModel =
            ViewModelProvider(this).get(PostViewModel::class.java)
        fragmentPostBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_post, container, false)


        return fragmentPostBinding.root
    }
}