package com.bhaleraoakshay.task_vadsoft.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bhaleraoakshay.task_vadsoft.R
import com.bhaleraoakshay.task_vadsoft.retrofit_utils.model.user.UserResponse
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.layout_user_profile_item.view.*

class UserAdapter( private val listener: (UserResponse) -> Unit) : PagedListAdapter<UserResponse, UserAdapter.UserViewHolder>(USER_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_user_profile_item, parent, false)
        return UserViewHolder(view)
    }


    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val user = getItem(position)
        if (user != null) {
            holder.bind(user,listener)
        }



    }


    class UserViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val imageView = view.user_avatar
        private val userName = view.user_name
        private val userDesignation = view.user_designation
        private val userCity = view.textView_UserCity
        fun bind(
            user: UserResponse,
            listener: (UserResponse) -> Unit
        ) {
            userName.text = user.name
            userDesignation.text = user.designation
            userCity.text = user.city
            Glide.with(imageView.context)
                .load(user.avatar)
                .placeholder(R.drawable.ic_loading)
                .into(imageView);

            itemView.setOnClickListener { listener(user)}
        }

    }

    companion object {
        private val USER_COMPARATOR = object : DiffUtil.ItemCallback<UserResponse>() {
            override fun areItemsTheSame(oldItem: UserResponse, newItem: UserResponse): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: UserResponse, newItem: UserResponse): Boolean =
                newItem == oldItem
        }
    }


}

    class UserViewHolder {

    }

