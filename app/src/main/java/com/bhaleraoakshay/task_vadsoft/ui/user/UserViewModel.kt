package com.bhaleraoakshay.task_vadsoft.ui.user

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.bhaleraoakshay.task_vadsoft.retrofit_utils.model.user.UserDataSource
import com.bhaleraoakshay.task_vadsoft.retrofit_utils.model.user.UserDataSourceFactory
import com.bhaleraoakshay.task_vadsoft.retrofit_utils.model.user.UserResponse

class UserViewModel : ViewModel() {
    var userPagedList: LiveData<PagedList<UserResponse>>
    private var liveDataSource: LiveData<UserDataSource>
    init {
        val itemDataSourceFactory =
            UserDataSourceFactory()
        liveDataSource = itemDataSourceFactory.userLiveDataSource
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(UserDataSource.PAGE_SIZE)
            .build()
        userPagedList = LivePagedListBuilder(itemDataSourceFactory, config)
            .build()
    }


}